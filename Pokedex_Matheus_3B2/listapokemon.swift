//
//  listapokemon.swift
//  Pokedex_Matheus_3B2
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit
struct pokemon {
    var nome: String
    var tipo1: String
    var tipo2: String
    var imagem: String
    var cor: String
    
}
class listapokemon: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var pokemons: [pokemon] = [
        pokemon(nome: "Bulbassauro", tipo1: "Grama", tipo2: "Venenoso", imagem: "bulbassauro", cor: "verde"),
        pokemon(nome: "Ivyssauro", tipo1: "Grama", tipo2: "Venenoso", imagem: "ivysaur", cor: "verde"),
        pokemon(nome: "Venussauro", tipo1: "Grama", tipo2: "Venenoso", imagem: "venusaur", cor: "verde"),
        pokemon(nome: "Charmander", tipo1: "Fogo", tipo2: "", imagem: "charmander", cor: "vermelho"),
        pokemon(nome: "Charmeleon", tipo1: "Fogo", tipo2: "", imagem: "charmelon", cor: "vermelho"),
        pokemon(nome: "Charizard", tipo1: "Fogo", tipo2: "", imagem: "charizard", cor: "vermelho"),
        pokemon(nome: "Squirtle", tipo1: "Água", tipo2: "", imagem: "squirtle", cor: "azul"),
        pokemon(nome: "Wartortle", tipo1: "Água", tipo2: "", imagem: "wartortle", cor: "azul"),
        pokemon(nome: "Blastoise", tipo1: "Água", tipo2: "", imagem: "blastoise", cor: "azul"),
        pokemon(nome: "Pikachu", tipo1: "Elétrico", tipo2: "", imagem: "Pikachu", cor: "amarelo")
    ]
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "celula") as? TableViewCell else{return UITableViewCell()}
            let pokemon = pokemons[indexPath.row]
            cell.nome.text = pokemon.nome
            cell.tipo1.text = pokemon.tipo1
            cell.tipo2.text = pokemon.tipo2
            cell.imagem.image = UIImage(named: pokemon.imagem)
            cell.contentView.backgroundColor = UIColor(named: pokemon.cor)
        if pokemon.tipo2 == "" {
            cell.fundoView.backgroundColor = UIColor(named: pokemon.cor)
        }else{
            cell.fundoView.backgroundColor = UIColor(named: "fundoTransparente")
        }

        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
